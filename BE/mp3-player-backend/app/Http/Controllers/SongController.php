<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;

class SongController extends Controller {
    
    public function displaySongs(){
        return Song::all();
    }
    
    public function uploadSong(Request $request) {
        $new_song = new Song();
        $new_song->title = $request->title;
        $new_song->length = $request->length;
        $new_song->artist = $request->artist;
        $new_song->save();
        return response()->json(['message'=>'Song added successful'], 200);
    }
}
