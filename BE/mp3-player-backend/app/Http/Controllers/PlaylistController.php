<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;    
use App\Models\Playlist;

class PlaylistController extends Controller {

    public function displayPlaylists(){
        return Playlist::all();
    }

    public function createPlaylist(Request $request) {
        $playlist = new Playlist(); 
        $playlist->name = $request->name;
        $playlist->save();
        return response()->json(['message'=>'Playlist added successful'], 200);
    }
}
