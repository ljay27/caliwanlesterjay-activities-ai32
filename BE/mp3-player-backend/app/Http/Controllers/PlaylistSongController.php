<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PlaylistSong;

class PlaylistSongController extends Controller {
    
    public function displayPlaylistSongs() {
        return PlaylistSong::all();
    }
    
    public function addSong(Request $request) {
        $song = new PlaylistSong(); 
        $song->song_id = $request->song_id;
        $song->playlist_id = $request->playlist_id;
        $song->save();
        return response()->json(['message'=>'Successfuly added song in the playlist'], 200);
    }
}
